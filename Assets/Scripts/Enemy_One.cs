﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_One : Enemy
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if(Mathf.Abs(targetDistance) < attackDistance && Mathf.Abs(targetDistance) > attackDistance - 9)
        {
            anim.SetBool("Attack", false);
            anim.SetBool("Move", true);
            if(target.position.x < transform.position.x && facingRight)
            {
                Flip();
            }
            if (target.position.x > transform.position.x && !facingRight)
            {
                Flip();
            }
            Vector3 targetX= target.transform.position;
            targetX.y = transform.position.y;
            transform.position = Vector3.MoveTowards(transform.position, targetX, speed * Time.deltaTime);
        }
        else if(Mathf.Abs(targetDistance) < attackDistance - 9)
        {
            anim.SetBool("Move", false);
            anim.SetBool("Attack", true);
        }
        else
        {
            anim.SetBool("Move", false);
            anim.SetBool("Attack", false);
        }
    }

}