﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{

    public int health;
    //how fast the character can run
    public float topSpeed = 10.0f;
    //tell the sprite which direction it is facing
    bool facingRight = true;

    //get reference to animator
    Animator anim;

    //not grounded
    public bool grounded = false;

    //transform at character foot to see if it is touching the ground
    public Transform groundCheck;

    //how big the circle is going to be when we check distance to the ground
    float groundRadius = 0.2f;

    //force of the jump
    public float jumpForce = 300f;

    //what layer is considered ground
    public LayerMask whatIsGround;

    //sec jump
    public bool secJump = false;

    private void Start()
    {
        health = 10;
        //anim = GetComponent<Animator>();
    }


    //physics in fixed update
    private void FixedUpdate()
    {

        //true or false did the ground transform hit whatIsGround with the groundRadius
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);

        //tell the animator that we are grounded
        //anim.SetBool("Ground", grounded);

        //get how fast we are moving up or down from the rigid body
        //anim.SetFloat("vSpeed", GetComponent<Rigidbody2D>().velocity.y);

        //get move direction
        float move = Input.GetAxis("Horizontal");

        //add velocity to the rigidbody in the move direction * current speed
        GetComponent<Rigidbody2D>().velocity = new Vector2(move * topSpeed, GetComponent<Rigidbody2D>().velocity.y);

        //anim.SetFloat("Speed", Mathf.Abs(move));

        //if we are facing the negative direction and not facing right, flip
        if (move > 0 && !facingRight)
            Flip();
        else if (move < 0 && facingRight)
            Flip();
    }

    void Update()
    {
        if ((grounded || secJump) && Input.GetKeyDown(KeyCode.Space))
        { 
            secJump = grounded;

            //not on ground
            //anim.SetBool("Ground", false);
            GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
        }
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);

    }

    void Flip()
    {
        //facing oposite direction
        facingRight = !facingRight;

        //get the local scale
        Vector3 theScale = transform.localScale;

        //flip on x axis
        theScale.x *= -1;

        //apply that to the local scale
        transform.localScale = theScale;
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);

        if (hitInfo != null && hitInfo.tag == "Enemy")
        {
            Debug.Log("HitEnemy");
            health -= 1;
            if(health <= 0)
            {
                Destroy(gameObject);
            }
            
        }
       
    }
}
