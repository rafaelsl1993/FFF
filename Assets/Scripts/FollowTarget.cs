﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    // what we are following
    public Transform target;

    // zeroes out the velocity
    Vector3 velocity = Vector3.zero;

    // time to follow target
    public float smoothTime = .15f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // target position
        Vector3 targetPos = target.position;
        targetPos.y += 2.8f;
        if (targetPos.y > 4.5f)
        {
            targetPos.y = 4.5f;
        }
        // align the camera and the target's z position
        targetPos.z = transform.position.z;

        // using smooth damp we will gradually change the camera transform position to the target position based on the cameras transform velocity and our smooth time
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref velocity, smoothTime);
    }
}
