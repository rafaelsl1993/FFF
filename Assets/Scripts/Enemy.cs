﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    public float attackDistance;
    public int health;

    public GameObject deathAnimation;

    protected Animator anim;
    protected bool facingRight = true;
    public Transform target;
    protected float targetDistance;
    protected Rigidbody2D rb2d;
    protected SpriteRenderer sprite;


    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        target = GameObject.Find("Character").transform;
        //Debug.Log("Target: " + target.name);
        rb2d = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        targetDistance = transform.position.x - target.position.x;
        //Debug.Log("TargetDistance: " + targetDistance);

    }

    protected void Flip()
    {
        //facing oposite direction
        facingRight = !facingRight;

        //get the local scale
        Vector3 theScale = transform.localScale;

        //flip on x axis
        theScale.x *= -1;

        //apply that to the local scale
        transform.localScale = theScale;
    }

    public void Hit()
    {
        health -= 1;

        if (health <= 0)
        {
            Destroy(gameObject, 1);

            anim.SetBool("Move", false);
            anim.SetBool("Attack", false);
            Flip();
            anim.SetBool("Death", true);
            //Instantiate(deathAnimation, transform.position, transform.rotation);
            //gameObject.SetActive(false);

        }
        else
        {
            StartCoroutine(TookDamageCoRoutine());
        }

    }

    IEnumerator TookDamageCoRoutine()
    {
        sprite.color = Color.red;

        yield return new WaitForSeconds(0.1f);

        sprite.color = Color.white;

    }
}
