﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{

    public Transform target;
    private Vector3 position;
    public float paralaxEffect;

    // Start is called before the first frame update
    void Start()
    {
        position = target.position;

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate((target.position.x - position.x) / paralaxEffect, (target.position.y - position.y) / paralaxEffect, 0);
        position = target.position;

    }
}
