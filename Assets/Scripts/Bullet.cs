﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 70f;
    public Rigidbody2D rb;
    public float xS=1;

    // Start is called before the first frame update
    void Start()
    {
        float xAxis = Input.GetAxis("Horizontal");
        float yAxis = Input.GetAxis("Vertical");

        if (yAxis != 0)
        {
            rb.velocity = new Vector2(0, 1 * yAxis) * speed;
        }
        else
        {
            rb.velocity = new Vector2(1 * xS, 0) * speed * 2;
        }

        
    }
       
    
    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        Debug.Log(hitInfo.name);

        Enemy otherEnemy = hitInfo.GetComponent<Enemy>();
        if(hitInfo != null && hitInfo.tag == "Enemy")
        {
            Debug.Log("HitEnemy");
            otherEnemy.Hit();
            Destroy(gameObject);
        }
        if (hitInfo.name != "Character")
        {
            Destroy(gameObject);
        }
    }
}
